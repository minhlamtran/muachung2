class Admin::CategoriesController < ApplicationController
  before_action :check_authorize, only: [:show, :edit, :update, :destroy]
  def index
    authorize Category
  	@categories = Category.all.order(parent_id: :ASC).page params[:page]
  end

  def show
    
  end

  def new
  	@category = Category.new
    authorize @category
  	@categories_parent = Category.where(parent_id: "0")
  	@i = 0
  end

  def edit
  	@i = 0  	
  	@categories_parent = Category.where(parent_id: "0").order("id = #{@category.parent_id} DESC")
  end

  def create
  	@category = current_user.categories.new(category_params)
    authorize @category
  	if @category.save
      flash[:success] = "create new category success!"
  		redirect_to admin_category_path(@category)
  	else
      flash[:error] = "Fail! Please try again!"
  		render :new
  	end
  end

  def update
    if @category.update(category_params)
      flash[:success] = "Uppdate success!"
      redirect_to admin_category_path(@category)
    else
      flash[:error] = "Fail! Please try again!"
      render :edit
    end
  end

  def destroy
    if @category.destroy
      flash[:success] = "Uppdate success!"
    else
      flash[:error] = "Fail! Please try again!"
    end

    redirect_to admin_categories_path
  end

  private

  def check_authorize
    @category = Category.find(params[:id])
    authorize @category
  end
  def category_params
  	params.require(:category).permit(:name, :parent_id, :description, :status, :order)
  end

end
