class Admin::ProductsController < ApplicationController
  before_action :check_authorize, only: [:show, :update, :destroy, :edit]
  def index
    authorize Product
    @products = Product.all.page params[:page]    
  end

  def show
  end

  def new
  	@product = Product.new
    @categories_lv1 = Category.where(:parent_id => "0")
    authorize @product
  end

  def edit
  	@categories_lv1 = Category.where(parent_id: "0").order("id = #{@product.category_id} DESC")
  end

  def create
  	@product = current_user.products.new(product_params)
    authorize @product
  	if @product.save
  		flash[:notice] = "Create new product success"
  		redirect_to admin_product_path(@product)
  	else
  		flash[:danger] = "Fail!"
  		render :new
  	end
  end

  def update
  	if @product.update(product_params)
  		flash[:success] = "Update success"
  		redirect_to admin_product_path(@product)
  	else
  		flash[:danger] = "Fail!"
  		render :edit
  	end
  end

  def destroy
  	if @product.destroy
  		flash[:success] = "Deleted!"
  	else
  		flash[:danger] = "Fail!"
  	end
  	redirect_to admin_products_path
  end

  private

  def check_authorize
    @product = Product.find(params[:id])
    authorize @product  
  end

  def product_params
  	params.require(:product).permit(:name, :price, :banner, :total, :category_id, :status, :description, :sale_off, :sale_off_type, :images, :created_by)
  end
end
