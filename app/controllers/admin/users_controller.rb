class Admin::UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :check_authorize, only: [:show, :edit, :update, :destroy]
  def index
    @users = User.all.page params[:page]
  	authorize User
  end

  def show
  	
  end

  def new
  	@user = User.new
    authorize @user
  end

  def edit
  end

  def create
  	@user = User.new(user_params)
  	authorize @user
  	if @user.save
      flash[:success] = "Create new User success!"
  		redirect_to admin_user_path(@user)
  	else
      flash[:error] = "Fail! Please try again!"
  		render :new
  	end
  end

  def update
    if @user.update(user_params)
      flash[:success] = "Uppdate success!"
      redirect_to admin_user_path(@user)
    else
      flash[:error] = "Fail! Please try again!"
      render :edit
    end
  end

  def destroy
    if @user.destroy
      flash[:success] = "Uppdate success!"
    else
      flash[:error] = "Fail! Please try again!"
    end

    redirect_to admin_users_path
  end

  private

  def check_authorize
    @user = User.find(params[:id])
    authorize @user
  end

  def user_params
  	params.require(:user).permit(:email, :role, :fullname, :password)
  end
end
