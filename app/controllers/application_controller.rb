class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  before_action :set_locale
  def set_locale
   I18n.locale = params[:locale] || I18n.default_locale
  end

  include Pundit
  protect_from_forgery

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_filter :set_locale
  private
  def user_not_authorized(exception)
  	policy_name = exception.policy.class.to_s.underscore

    flash[:error] = t "#{policy_name}.#{exception.query}",
  	scope: "pundit", default: :default
    redirect_to(request.referrer || root_path)
  end

  def set_locale
	  I18n.locale = params[:locale] || I18n.default_locale
	end

end
