class CartController < ApplicationController
  def index

  end

  def add_to_cart  	
  	if session["cart"].nil?
  		session["cart"] = []
  	end
  	unless params[:id].nil?
    	session["item"] = {:id => params[:id].to_i, :quantity => params[:quantity].to_i}
    	check_cart session["item"]       
    end
  end

  def update_quantity_to_cart
    product_id_ = params[:id].to_i
    return if session["cart"].nil?
    session["cart"].each do |h|      
      if h['id'] == product_id_
        h['quantity'] = params[:quantity_update].to_i
      end
    end    
  end

  def delete_product_to_cart
    return if params[:id].nil?
    product_id_ = params[:id].to_i
    session["cart"].each do |h|      
      if h['id'] == product_id_
        session["cart"].delete(h)
      end
    end    
  end


  private

  def check_cart(item)

  	session["cart"].each do |x|
  		if x['id'] == item[:id]
  			x['quantity'] += item[:quantity].to_i
  			return 
  		end
  	end

  	session["cart"] << item
  end
end
