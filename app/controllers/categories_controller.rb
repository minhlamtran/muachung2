class CategoriesController < ApplicationController
  skip_before_action :authenticate_user!
  
  def show
  	@category = Category.find(params[:id])
  	@category_children = @category.children
  	@categories_other = Category.where(:parent_id => '0', :status => "active").where.not(:id => params[:id]).order(order: :ASC)
  	
  	@products = Product.where("category_id in (?)", @category.children.map(&:id) << @category.id ).where(:status => "active")
   
  	@product_slideshow_single = Product.where(:banner => "single", :status => "active").order(sold: :DESC).limit(4)
  	@product_slideshow_double = Product.where(:banner => "double", :status => "active").order(sold: :DESC).limit(4)

  	@product_slideshow = []
  	@product_slideshow_double.each_with_index do |product, index|
  		@product_slideshow << product

  		@product_slideshow_single.each_with_index do |product2, index2|
  			if index == index2
  				@product_slideshow << product2
	  			break
	  		end
	  	end
  	end
  end
end
