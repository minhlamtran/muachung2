class HomeController < ApplicationController
  skip_before_action :authenticate_user!

  def index
  	@categories_lv1 = Category.where(:parent_id => 0, :status => "active").order(order: :ASC)
  	
  	@products = []
  	@categories_lv1.each_with_index do | category, index |
  		
      category_id_list = category.children.map(&:id) << category
	  	@products_group = Product.where("category_id in (?)", category_id_list).where(:banner => "single", :status => "active").limit(10)
    
      @product_double = Product.where("category_id in (?)", category_id_list).where(:banner => "double", :status => "active").first
      
      if (@products_group.count > 3) && !@product_double.nil?
        @products_group.to_a.insert(4, @product_double) 
      end
      
    @products << @products_group

	 	
  	end
  end
end
