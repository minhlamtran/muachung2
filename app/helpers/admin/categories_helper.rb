module Admin::CategoriesHelper
	def show_category_parent(id)
		@string = id.to_s
		return @string += " (none)" if id == 0
		
	  	@category = Category.find(id)		  	
		return @string += " (" + @category.name.to_s + ")" if @category.respond_to?("name")
	end

	def show_category_name(id)
		return @string += "(none)" if id == 0		
	  	@category = Category.find(id)
	  	@category.name
	end

	def find_categories_lv2(id)
		return false if id == 0
		@categories_lv2 = Category.where(:parent_id => "#{id}")
	end
end
