module Admin::ProductsHelper
	def money_format(param)
		number_to_currency(param, unit: 'đ', delimiter: '.',precision: 0, format: '%n %u')
	end
end
