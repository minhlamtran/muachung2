module HomeHelper
	def money_format(param)
		number_to_currency(param, unit: 'đ', delimiter: '.',precision: 0, format: '%n %u')
	end

	def price_now_integer(price, sale_off, sale_off_type)
		case sale_off_type	

		when "%"
			@pr_now = price.to_f*( 100 - sale_off.to_f)/100
		when "vnd"
			@pr_now = price.to_f - sale_off.to_f 
		else
			@pr_now = price.to_f
		end

		@pr_now.to_i
	end

	def price_now(price, sale_off, sale_off_type)
		# case sale_off_type
		# when "%"
		# 	@pr_now = price.to_f*( 100 - sale_off.to_f)/100
		# when "vnd"
		# 	@pr_now = price.to_f - sale_off.to_f 
		# else
		# 	@pr_now = price.to_f
		# end
		self::price_now_integer(price, sale_off, sale_off_type)
		self::money_format(@pr_now)
	end

	def sale_off_value(price, sale_off, sale_off_type)
		case sale_off_type
		when "%"
			@value = price.to_f*sale_off.to_f/100
		when "vnd"
			@value = sale_off
		end

		self::money_format(@value)
	end

	
end
