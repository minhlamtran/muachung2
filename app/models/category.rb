class Category < ActiveRecord::Base
	 paginates_per(20)
	 belongs_to :user
	 has_many :products
	 has_many :children, class_name: "Category",
                          foreign_key: "parent_id"
 
	 belongs_to :parent, class_name: "Category"
	 # def parent
	 # 	self.parent_id == 0 ? nil : Category.where(id: self.parent_id ).first
	 # end

	 # def children
	 # 	Category.where(parent_id: self.id )#.first
	 # end

	 def self.policy_class
	    Admin::CategoryPolicy
	 end
end
