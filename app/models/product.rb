class Product < ActiveRecord::Base
	paginates_per(20)
	mount_uploader :images, ImagesUploader
	belongs_to :user
	belongs_to :category	
	def self.policy_class
    	Admin::ProductPolicy
  	end
end
