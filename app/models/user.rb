class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :products
  has_many :categories

  paginates_per(10)
  
  def self.policy_class
    Admin::UserPolicy
  end

  def admin?
  	role == "admin"
  end

  def support?
  	role == "aupport"
  end
end
