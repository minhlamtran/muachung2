class Admin::ProductPolicy < ApplicationPolicy
	def index?
		user.support? || user.admin?
	end

	def create?
		index?
	end

	def show?
		index?
	end

	def new?
		index?
	end

	def edit?
		index?
	end

	def update?
		index?
	end

	def destroy?
		index?
	end
end