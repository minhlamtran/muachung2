Rails.application.routes.draw do

  root 'home#index'

  
  resources :categories
  resources :products
  devise_for :users, controllers: { registrations: 'users/registrations',
                                    sessions:       'users/sessions'
  }

  # Example resource route within a namespace:
  namespace :admin do
    # Directs /admin/products/* to Admin::ProductsController
    # (app/controllers/admin/products_controller.rb)
    resources :products, :users, :categories
    # root "users#index"
    get 'dashboard/index'
  end

  # scope module: 'user' do
  #   get 'users/index'
  # end 
  match 'users/index', to: 'users/users#index', via: [:get]

  # resource :carts, only: [:index, :show]
  # get 'cart/index'
  resources :cart, only: [:index] do
    collection do
      get 'add_to_cart'
      get 'update_quantity_to_cart'
      get 'delete_product_to_cart'
    end
  end



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable
    
end
