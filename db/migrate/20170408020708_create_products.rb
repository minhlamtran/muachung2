class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :price
      t.string :sale_off
      t.string :sale_off_type
      t.string :description

      t.timestamps null: false
    end
  end
end
