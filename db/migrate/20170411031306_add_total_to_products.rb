class AddTotalToProducts < ActiveRecord::Migration
  def change
    add_column :products, :total, :integer
  end
end
