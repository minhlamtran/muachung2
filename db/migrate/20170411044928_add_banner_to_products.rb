class AddBannerToProducts < ActiveRecord::Migration
  def change
    add_column :products, :banner, :string, :default => "single"
  end
end
