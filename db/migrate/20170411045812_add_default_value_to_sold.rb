class AddDefaultValueToSold < ActiveRecord::Migration
  def change
  	change_column :products, :sold, :integer, :default => 0
  end
end
