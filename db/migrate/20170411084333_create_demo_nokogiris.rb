class CreateDemoNokogiris < ActiveRecord::Migration
  def change
    create_table :demo_nokogiris do |t|
      t.string :title
      t.text :content

      t.timestamps null: false
    end
  end
end
