class RemoveCreatedByFromProducts < ActiveRecord::Migration
  def change
  	remove_column :products, :created_by
  end
end
