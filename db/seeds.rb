# encoding: UTF-8
# This file is auto-generated from the current content of the database. Instead
# of editing this file, please use the migrations feature of Seed Migration to
# incrementally modify your database, and then regenerate this seed file.
#
# If you need to create the database on another system, you should be using
# db:seed, not running all the migrations from scratch. The latter is a flawed
# and unsustainable approach (the more migrations you'll amass, the slower
# it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Base.transaction do

    status_list     = ["active", "active", "active", "active", "active", "active", "active", "active", "inactive", "pending", "banned"]
    status_count    = status_list.count

    #create database users
    User.delete_all

    User.create(fullname: "admin", email: "admin@admin.com", password: "zxzxzx", role: "admin", status: "active")
    User.create(fullname: "support", email: "support@support.com", password: "zxzxzx", role: "support", status: "active")
    User.create(fullname: "user", email: "user@gmail.com", password: "zxzxzx", role: "member", status: "active")

    user_id_list    = User.where("role in (?)", ["admin", "support"]).map(&:id)
    user_id_count   = user_id_list.count

    20.times do |i|
        User.create(
            fullname: "user #{i}", 
            email: "usertest#{i}@gmail.com", 
            password: "zxzxzx",
            role: "member",
            status: status_list[rand(status_count)]
        )
    end

    #create database categories
    Category.delete_all

    10.times do |i|
        Category.create(
            name: "Category parent No.#{i}",
            parent_id: 0, 
            description: nil,
            status: status_list[rand(status_count)], 
            order: rand(1..i), 
            user_id: user_id_list[rand(user_id_count)]
        )        
    end

    cate_id_parent_list    = Category.where(:parent_id => 0).map(&:id)    

    cate_id_parent_list.each do |id|
        10.times do |i|
            Category.create(
                name: "Category Child #{id} -> #{i}",
                parent_id: id, 
                description: nil,
                status: status_list[rand(status_count)], 
                order: rand(1..i), 
                user_id: user_id_list[rand(user_id_count)]
            )        
        end
    end

    
    #create database products
    Product.delete_all

    cate_id_list    = Category.where.not(:parent_id => 0).map(&:id)
    cate_id_count   = cate_id_list.count

    sale_off_type_list   = ["(none)", "%"]
    sale_off_type_count  = sale_off_type_list.count

    banner_type_list    = ["single","double"]
    banner_type_count   = banner_type_list.count

    200.times do |i|
    	p = Product.create(
    		name: "Data Migration Seed Demo Product Test #{i}", 
    		price: rand(1..10)*100000, 
    		sale_off: rand(25..50), 
    		sale_off_type: sale_off_type_list[rand(sale_off_type_count)], 
    		description: "updating...", 
    		sold: rand(5..100), 
    		status: status_list[rand(status_count)], 
    		banner: banner_type_list[rand(banner_type_list.count)], 
    		category_id: cate_id_list[rand(cate_id_count)], 
    		user_id: user_id_list[rand(user_id_list.count)] )
        if p.banner == "single"
        	p.remote_images_url = "https://muachungcdn.com/thumb_w/550,80/i:product/170/3/lp1y0/set-steak-bo-my-va-nhieu-mon-kieu-au-cho-4-5-nguoi.jpg"
        else
            p.remote_images_url = "https://muachungcdn.com/thumb_w/550,80/product/thumbnail/2017/02/27/2rrlc.jpg"
        end
    	p.save
    end

end

SeedMigration::Migrator.bootstrap(20170420020745)
